import React, { Component } from "react";
import Home from "./components/main";
import "./App.css";

import { BrowserRouter} from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <Home />
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
