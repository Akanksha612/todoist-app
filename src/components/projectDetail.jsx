import React, { Component } from "react";
import axios from "axios";
import { v4 as uuidv4 } from "uuid";
import { Modal, Button, Dropdown } from "react-bootstrap";

import "../App.css";

class Project extends Component {
  state = {
    data: [],
    showAdd: true,
    showForm: false,
    input: " ",
    delete: false,
  };

  componentDidMount() {
    let options = {
      method: "GET",
      headers: {
        Authorization: "Bearer 9c797655e38315b4aca264ee68f25c06e07ba22e",
      },
    };

    axios
      .get("https://api.todoist.com/rest/v1/tasks", options)
      .then((resp) => this.setState({ data: resp.data }));
  }

  //Collecting all tasks objects who have project_id==project_id of clicked project

  FilteredArray = () => {
    let project_value = this.props.location.hello.lovely.object.id;
    let arr = this.state.data.filter((item) => {
      return (item.project_id === project_value && item.completed===false);
    });

    return arr;
  };

  handleForm = () => {
    this.setState({ showAdd: false, showForm: true });
  };

  handleCancel = () => {
    this.setState({ showAdd: true, showForm: false });
  };

  handleChange = (e) => {
    this.setState({ input: e.target.value });
  };

  handleShowDelete = () => {
    this.setState({ delete: true });
  };

  handleCloseDelete = () => {
    this.setState({ delete: false });
  };

  handleDelete = (id) => {
    console.log("hellooooo");
    console.log(id);

    this.setState({ delete: false });

    let headers = {
      Authorization: "Bearer 9c797655e38315b4aca264ee68f25c06e07ba22e",
    };

    axios
      .delete(`https://api.todoist.com/rest/v1/tasks/${id}`, { headers })
      .then((resp) => {
        let arr = this.state.data.filter((task) => {
          return task.id !== id;
        });

        this.setState({ data: arr });
      });
  };

  handleChecked = (id) => {
    const headers = {
      Authorization: "Bearer 9c797655e38315b4aca264ee68f25c06e07ba22e",
    };
    let task = this.state.data.find((task) => task.id === id);
    let url = "";
    if (task.completed) {
      url = `https://api.todoist.com/rest/v1/tasks/${id}/reopen`;
    } else {
      url = `https://api.todoist.com/rest/v1/tasks/${id}/close`;
    }
    axios
      .post(url, null, {
        headers,
      })
      .then(() => {
        this.setState({
          data: this.state.data.map((task) => {
            if (task.id === id) {
              return {
                ...task,
                completed: !task.completed,
              };
            }
            return task;
          }),
        });
      });
  };

  handleActiveTasks = () => {
    let arr = this.state.data.filter((task) => {
      return (task.project_id=== this.props.location.hello.lovely.object.id &&  task.completed===false) 
    });

    return arr;
    
  };

   
  handleCompletedTasks = () => {
    let arr = this.state.data.filter((task) => {
      return (task.project_id=== this.props.location.hello.lovely.object.id &&  task.completed!==false) 
    });

    return arr;
  };






  handleTaskAddition = async (e, id) => {
    // console.log(e,id)

    e.preventDefault();

    try {
      if (this.state.input !== "") {
        // const article = { name: ${this.state.userInput} };
        const article = { content: `${this.state.input}`, project_id: `${id}` };
        const headers = {
          Authorization: "Bearer 9c797655e38315b4aca264ee68f25c06e07ba22e",
          "Content-Type": "application/json",
          "X-Request-Id": `${uuidv4()}`,
        };
        const response = await axios.post(
          "https://api.todoist.com/rest/v1/tasks",
          article,
          { headers }
        );
        this.setState({
          data: [...this.state.data, response.data],
        });
      }
    } catch (error) {
      console.error(error);
    }

    this.setState({input: " "})
    //e.target.value="";
  };

  showForm = () => {
    return (
      <div>
        <form id="add-app" className="d-flex flex-column">
          <input
            type="text"
            value={this.state.input}
            onChange={this.handleChange}
            placeholder="Task name"
          />
          <footer className="d-flex justify-content-around ">
            <button
              onClick={(e) => {
                this.handleTaskAddition(
                  e,
                  this.props.location.hello.lovely.object.id
                );
              }}
            >
              Add task
            </button>
            <button onClick={this.handleCancel}>Cancel</button>
          </footer>
        </form>
      </div>
    );
  };

  render() {
    let name = this.props.location.hello.lovely.object.name;
    //console.log(name)
    //console.log(this.props.location.hello.lovely.object)
    console.log(this.state.data);

    let array = this.FilteredArray();
    let active_array= this.handleActiveTasks();
    console.log(active_array)
    let completed_array= this.handleCompletedTasks();
    console.log(completed_array)
    //console.log(array);



    return (
      <div className="d-flex flex-column align-items-center info-container m-5">
        {/* <div className="d-flex justify-content-between"> */}

        <h2> {name}</h2>

        <ul>
          {active_array.map((item) => {
            console.log(item);
            return (
              <div className="d-flex info-line">
                <li>
                  <input
                    type="checkbox"
                    className="checkbox m-2"
                    checked={item.completed}
                    onChange={() => {
                      this.handleChecked(item.id);
                    }}
                  />
                  {item.content}
                </li>

                <Dropdown className="dropdown">
                  <Dropdown.Toggle
                    variant="success"
                    id="dropdown-basic"
                    className="toggle"
                  >
                    ...
                  </Dropdown.Toggle>

                  <Dropdown.Menu>
                    <Dropdown.Item onClick={this.handleShowDelete}>
                      Delete task
                    </Dropdown.Item>

                    <Modal
                      show={this.state.delete}
                      onHide={this.handleCloseDelete}
                    >
                      <Modal.Header closeButton>
                        <Modal.Title>
                          Are you sure you want to delete?
                        </Modal.Title>
                      </Modal.Header>

                      <Modal.Footer>
                        <Button
                          variant="secondary"
                          onClick={this.handleCloseDelete}
                        >
                          Cancel
                        </Button>
                        <Button
                          variant="primary"
                          onClick={() => {
                            this.handleDelete(item.id);
                          }}
                        >
                          Okay
                        </Button>
                      </Modal.Footer>
                    </Modal>
                  </Dropdown.Menu>
                </Dropdown>
              </div>
            );
          })}
        </ul>

        {this.state.showAdd ? (
          <p>
            <button onClick={this.handleForm}>+</button>{" "}
            <span> Add a task</span>
          </p>
        ) : null}

        {array.length ? null : (
          <div>
            <img src="/todoist.png" alt="loading...." />
          </div>
        )}

        {this.state.showForm ? this.showForm() : null}



        <div>

        <ul>
          {completed_array.map((item) => {
            console.log(item);
            return (
              <div className="d-flex">
                <li>
                  <input
                    type="checkbox"
                    className="checkbox m-2"
                    checked={item.completed}
                    onChange={() => {
                      this.handleChecked(item.id);
                    }}
                  />
                  {item.content}
                </li>

                <Dropdown className="dropdown">
                  <Dropdown.Toggle
                    variant="success"
                    id="dropdown-basic"
                    className="toggle"
                  >
                    ...
                  </Dropdown.Toggle>

                  <Dropdown.Menu>
                    <Dropdown.Item onClick={this.handleShowDelete}>
                      Delete task
                    </Dropdown.Item>

                    <Modal
                      show={this.state.delete}
                      onHide={this.handleCloseDelete}
                    >
                      <Modal.Header closeButton>
                        <Modal.Title>
                          Are you sure you want to delete?
                        </Modal.Title>
                      </Modal.Header>

                      <Modal.Footer>
                        <Button
                          variant="secondary"
                          onClick={this.handleCloseDelete}
                        >
                          Cancel
                        </Button>
                        <Button
                          variant="primary"
                          onClick={() => {
                            this.handleDelete(item.id);
                          }}
                        >
                          Okay
                        </Button>
                      </Modal.Footer>
                    </Modal>
                  </Dropdown.Menu>
                </Dropdown>
              </div>
            );
          })}
        </ul>


        </div>



    


      </div>
    );
  }
}

export default Project;
