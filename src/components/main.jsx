import React, { Component } from "react";
import { v4 as uuidv4 } from "uuid";

import "../App.css";
import axios from "axios";
import { Accordion, Modal, Button, Dropdown } from "react-bootstrap";
import Project from "./projectDetail";
import { Link, Route, Switch } from "react-router-dom";

class Home extends Component {
  state = {
    data: [],
    show: false,
    delete: false,
    input: "",
    isClicked: false,
  };

  componentDidMount() {
    let options = {
      method: "GET",
      headers: {
        Authorization: "Bearer 9c797655e38315b4aca264ee68f25c06e07ba22e",
      },
    };

    axios
      .get(`https://api.todoist.com/rest/v1/projects`, options)
      .then((resp) =>
        this.setState({
          data: resp.data.filter((item) => {
            return item.name !== "Inbox";
          }),
        })
      );
  }

  handleClose = () => this.setState({ show: false });
  handleCloseDelete = () => {
    this.setState({ delete: false });
  };

  handleDelete = (id) => {
    this.setState({ delete: false });

    let headers = {
      Authorization: "Bearer 9c797655e38315b4aca264ee68f25c06e07ba22e",
    };

    axios
      .delete(`https://api.todoist.com/rest/v1/projects/${id}`, { headers })
      .then((resp) => {
        let arr = this.state.data.filter((object) => {
          return object.id !== id;
        });

        this.setState({ data: arr });
      });
  };

  handleShow = () => this.setState({ show: true });
  handleShowDelete = () => this.setState({ delete: true });

  handleChange = (e) => {
    this.setState({ input: e.target.value });
    console.log(this.state);
  };

  //using async and await to handle uncaught promise error

  handleAddition = async () => {
    const article = { name: `${this.state.input}` };

    const headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer 9c797655e38315b4aca264ee68f25c06e07ba22e",
      "X-Request-Id": `${uuidv4()}`,
    };

    await axios
      .post("https://api.todoist.com/rest/v1/projects", article, { headers })
      .then((resp) =>
        this.setState({ data: [...this.state.data, resp.data], show: false })
      );
  };


handleSideBar=()=>{


  this.setState({isClicked: !this.state.isClicked})
}



  render() {
    return (
      <div>
        <nav className="navbar navbar-light bg-danger">
          <div className="container-fluid">
            <div className="d-flex">
              <p type="button" onClick={this.handleSideBar}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="20"
                  height="20"
                  fill="currentColor"
                  className="bi bi-list text-light m-2"
                  viewBox="0 0 16 16"
                >
                  <path
                    fill-rule="evenodd"
                    d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"
                  />
                </svg>
              </p>

              <Link to="/">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="20"
                  height="20"
                  fill="currentColor"
                  className="bi bi-house-door m-2 text-light"
                  viewBox="0 0 16 16"
                >
                  <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z" />
                </svg>
              </Link>
            </div>
          </div>
        </nav>
        <section className="d-flex">

          {this.state.isClicked? 
          <div className="left bg-light">
            <div className="d-flex">
              <Accordion className="accordian">
                <Accordion.Item>
                  <Accordion.Header className="header-projects">Projects</Accordion.Header>
                  <Accordion.Body>
                    {this.state.data.map((object, index) => {
                      console.log(object);
                      return (
                        <div className="d-flex justify-content-between">
                          <Link
                            to={{
                              pathname: `/projects/${object.id}`,
                              hello: { lovely: { object } },
                            }}
                          >
                            <div key={index}>{object.name}</div>
                          </Link>

                          <Dropdown className="dropdown">
                            <Dropdown.Toggle
                              variant="success"
                              id="dropdown-basic"
                              className="toggle"
                            >
                              ...
                            </Dropdown.Toggle>

                            <Dropdown.Menu>

                            {/* Delete button and it's corresponding modal */}

                              <Dropdown.Item>
                                <p className="bg-light" onClick={this.handleShowDelete}>
                                  delete
                                </p>
                                <Modal
                                  show={this.state.delete}
                                  onHide={this.handleCloseDelete}
                                >
                                  <Modal.Header closeButton>
                                    <Modal.Title>
                                      Are you sure you want to delete?
                                    </Modal.Title>
                                  </Modal.Header>

                                  <Modal.Footer>
                                    <Button
                                      variant="secondary"
                                      onClick={this.handleCloseDelete}
                                    >
                                      Cancel
                                    </Button>
                                    <Button
                                      variant="primary"
                                      onClick={() => {
                                        this.handleDelete(object.id);
                                      }}
                                    >
                                      Okay
                                    </Button>
                                  </Modal.Footer>
                                </Modal>
                              </Dropdown.Item>
                            </Dropdown.Menu>
                          </Dropdown>
                        </div>
                      );
                    })}
                  </Accordion.Body>
                </Accordion.Item>
              </Accordion>


           {/* Add button and it's corresponding modal */}
              <div>
                <button
                  className="btn btn-light fs-5 plus-button"
                  onClick={this.handleShow}
                >
                  +
                </button>

                <Modal show={this.state.show} onHide={this.handleClose}>
                  <Modal.Header closeButton>
                    <Modal.Title>Add Project</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <div className="d-flex flex-column">
                      <h4>Name</h4>
                      <input type="text" onChange={this.handleChange} />

                      <h4>Color</h4>

                      <select
                        className="filter"
                        onChange={this.handleRegion}
                        placeholder="Filter by Region"
                        name="regions"
                        id="regions"
                      >
                        <option value="none" selected disabled hidden>
                          Charcoal
                        </option>
                        <option value="africa">Berry Red</option>
                        <option value="america">Red</option>
                        <option value="asia">Orange</option>
                        <option value="europe">Yellow</option>
                        <option value="oceania">Green</option>
                      </select>
                    </div>


                    {/* add to favourites toggle switch */}

                    <div className="custom-control custom-switch">
                      <input
                        type="checkbox"
                        className="custom-control-input"
                        id="customSwitches"
                      />
                      <label
                        className="custom-control-label"
                        for="customSwitches"
                      >
                        Add to favourites
                      </label>
                    </div>
                  </Modal.Body>
                  <Modal.Footer>
                    <Button variant="secondary" onClick={this.handleClose}>
                      Close
                    </Button>
                    <Button variant="primary" onClick={this.handleAddition}>
                      Save Changes
                    </Button>
                  </Modal.Footer>
                </Modal>
              </div>
            </div>
          </div> : null}
          <div className="right d-flex flex-column  align-items-center">
            <Switch>
              <Route exact path="/projects/:handle" component={Project} />
            </Switch>
          </div>
        </section>
      </div>
    );
  }
}

export default Home;
